TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        bm.c \
        main.c

HEADERS += \
    bm.h
