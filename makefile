TARGET=boyer-moore-test

all: $(TARGET)

$(TARGET): bm.o main.o
	$(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@

clean:
	rm -f $(TARGET) *.o
