#include <stdio.h>
#include <string.h>
#include "bm.h"

int main() {
	char* pat = "ABCDE";
	char* txt = "XXXXXYYCDEABCDE";

	uint16_t pat_len = strlen(pat);
	uint16_t txt_len = strlen(txt);

	BmCtx* ctx = BoyerMooreCtxInit((uint8_t*)pat, pat_len);

	printf("Bad Character table\n");
	for (int i = 0; i < ALPHABET_SIZE; i++) {
		if (ctx->bmBc[i] != pat_len)
			printf("%d(%c) = %d\n", i, i, ctx->bmBc[i]);
	}
	printf("\n");

	printf("Good Suffix table\n");
	for (int i = 0; i < pat_len; i++) {
		printf("%d(%c) %d\n", i, pat[i], ctx->bmGs[i]);
	}
	printf("\n");

	char* found = BoyerMoore(pat, pat_len, txt, txt_len, ctx);

	if (found == NULL)
		printf("not found\n");
	else
		printf("found %ld\n", found - txt);

	BoyerMooreCtxDeInit(ctx);
}
